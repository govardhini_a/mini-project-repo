trigger JewelryTrigger on Jewelry__c (before insert, before update) {
    if(Trigger.isBefore && Trigger.isInsert){
        JewelryHandler.onBeforeInsert(Trigger.new);        
    }
    else if(Trigger.isBefore && Trigger.isUpdate){
        JewelryHandler.onBeforeUpdate(Trigger.oldMap, Trigger.newMap);        
    }
}