trigger CensusMemberTrigger on Census_Member__c (after insert, after update, after delete, before insert, before update) {

    if(Trigger.isAfter && Trigger.isInsert){
        CensusMemberHandler.onAfterInsert(Trigger.new);        
    }
    else if(Trigger.isAfter && Trigger.isUpdate){
        CensusMemberHandler.onAfterUpdate(Trigger.oldMap, Trigger.newMap);        
    }
    else if(Trigger.isAfter && Trigger.isDelete){
        CensusMemberHandler.onAfterDelete(Trigger.old);        
    }
    else if(Trigger.isBefore && Trigger.isInsert){
        CensusMemberHandler.onBeforeInsert(Trigger.new);
    }
    else if(Trigger.isBefore && Trigger.isUpdate){
        CensusMemberHandler.onBeforeUpdate(Trigger.new);
    }
}