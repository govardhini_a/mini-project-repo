/*
@ Class name        :   CensusMemberHandler.cls
@ Created by        :   Govardhini
@ Created on        :   03-01-2021
@ Token/Jira Ticket :   SLIV - 62
@ Description       :   Trigger on Census Member object. 
*/
public class CensusMemberHandler {
    public static void onAfterInsert(List<Census_Member__c> lstNewCensusMember){  
        CensusMemberHandler objCensusMemberHandler = new CensusMemberHandler();
        objCensusMemberHandler.insertCensusMember(lstNewCensusMember);
        objCensusMemberHandler.afterInsertCensusMember(lstNewCensusMember);
    }

    public static void onAfterUpdate(Map<Id, Census_Member__c> mapOldIdToCensusMember, Map<Id, Census_Member__c> mapNewIdToCensusMember){
        CensusMemberHandler objCensusMemberHandler = new CensusMemberHandler();
        objCensusMemberHandler.updateCensusMember(mapOldIdToCensusMember, mapNewIdToCensusMember);
        objCensusMemberHandler.afterUpdateCensusMember(mapOldIdToCensusMember, mapNewIdToCensusMember);
    }

    public static void onAfterDelete(List<Census_Member__c> lstOldCensusMember){
        CensusMemberHandler objCensusMemberHandler = new CensusMemberHandler();
        objCensusMemberHandler.deleteCensusMember(lstOldCensusMember);
    }

    public static void onBeforeInsert(List<Census_Member__c> lstNewCensusMember){
        CensusMemberHandler objCensusMemberHandler = new CensusMemberHandler();
        objCensusMemberHandler.beforeInsertCensusMember(lstNewCensusMember);
        objCensusMemberHandler.beforInsertOfCensusMemberWithPrimary(lstNewCensusMember);
    }

    public static void onBeforeUpdate(List<Census_Member__c> lstNewCensusMember){
        CensusMemberHandler objCensusMemberHandler = new CensusMemberHandler();
        objCensusMemberHandler.beforeInsertCensusMember(lstNewCensusMember);
        objCensusMemberHandler.beforInsertOfCensusMemberWithPrimary(lstNewCensusMember);
    }

/*
@ Method name       :   insertCensusMember
@ Created by        :   Govardhini
@ Created on        :   03-01-2021
@ Token/Jira Ticket :   SLIV - 62
@ Description       :   Task - 05 Insert Case. If a Census_Member__c object is Inserted with the Primary as Checked then,
the Census__c object's Primary Member Field Should be updated with the Primary Checked Census_Member's Name. 
*/
    public void insertCensusMember(List<Census_Member__c> lstNewCensusMember){
        // When I create a Census Member record, check if the “Primary” checkbox field is marked as True.
        // Then, update Census member Name to its parent Census field Primary Member.         
        Map<Id, String> mapCensusMemberNametoCensus = new Map<Id, String>();               
        for(Census_Member__c objNewCensusMember : lstNewCensusMember){
           if((objNewCensusMember.vlocity_ins_IsPrimaryMember__c == TRUE) && (objNewCensusMember.vlocity_ins_CensusId__c != NULL)){
                mapCensusMemberNametoCensus.put(objNewCensusMember.vlocity_ins_CensusId__c, objNewCensusMember.Name);            
           }
        }        
        List<Census__c> lstCensusToUpdate = new List<Census__c>();
        for(Id censusId : mapCensusMemberNametoCensus.keySet()){
            Census__c objCensus =new Census__c();
            objCensus.Id = censusId;
            objCensus.Primary_Member__c = mapCensusMemberNametoCensus.get(censusId);
            lstCensusToUpdate.add(objCensus);
        }       
        if(!lstCensusToUpdate.isEmpty()){
            update lstCensusToUpdate;
        }                
    }
    
/*
@ Method name       :   updateCensusMember
@ Created by        :   Govardhini
@ Created on        :   03-01-2021
@ Token/Jira Ticket :   SLIV - 62
@ Description       :   Task - 05 Update Case. If a Census_Member__c object is Updated with the Primary as Checked then,
the Census__c object's Primary Member Field Should be updated with the Primary Checked Census_Member's Name.
or if the Primary field is unchecked then the Primary Member Should be set to NULL.  
*/
    public void updateCensusMember(Map<Id, Census_Member__c> mapOldIdToCensusMember, Map<Id, Census_Member__c> mapNewIdToCensusMember){
        // When I Update a Census Member record, check if the “Primary” checkbox field is marked as True.
        // Then, update Census member Name to its parent Census field Primary Member.
        Map<Id, String> mapCensusMemberNametoCensus = new Map<Id, String>();
        List<Census__c> lstCensusToUpdate = new List<Census__c>();        
        for(Id CensusMemberId : mapNewIdToCensusMember.keySet()){
            if(mapOldIdToCensusMember.get(CensusMemberId).vlocity_ins_IsPrimaryMember__c == FALSE && mapNewIdToCensusMember.get(CensusMemberId).vlocity_ins_IsPrimaryMember__c == TRUE){
                Census__c objCensus =new Census__c();
                objCensus.Id = mapNewIdToCensusMember.get(CensusMemberId).vlocity_ins_CensusId__c;
                objCensus.Primary_Member__c = mapNewIdToCensusMember.get(CensusMemberId).Name;
                lstCensusToUpdate.add(objCensus);
            }
            else if(mapOldIdToCensusMember.get(CensusMemberId).vlocity_ins_IsPrimaryMember__c == TRUE && mapNewIdToCensusMember.get(CensusMemberId).vlocity_ins_IsPrimaryMember__c == FALSE){
                census__c objCensus = new census__c();
                objCensus.Id = mapNewIdToCensusMember.get(CensusMemberId).vlocity_ins_CensusId__c;
                objCensus.Primary_Member__c = NULL;
                lstCensusToUpdate.add(objCensus);
            }            
        }        
        if(!lstCensusToUpdate.isEmpty()){
            update lstCensusToUpdate;
        }                  
    }
/*
@ Method name       :   deleteCensusMember
@ Created by        :   Govardhini
@ Created on        :   03-01-2021
@ Token/Jira Ticket :   SLIV - 62
@ Description       :   Task - 05 Delete Case. If a Census_Member__c object is Deleted with the Primary as Checked then,
the Primary Member Should be set to NULL.  
*/
    public void deleteCensusMember(List<Census_Member__c> lstOldCensusMember){
        // if census Member is Deleted with the Primary Check box as True Then Update the Census.
        List<Census__c> lstCensusToUpdate = new List<Census__c>();
        for(Census_Member__c objOldCensusMember :lstOldCensusMember){
            if((objOldCensusMember.vlocity_ins_IsPrimaryMember__c == TRUE) && (objOldCensusMember.vlocity_ins_CensusId__c != NULL)){
            Census__c objCensus =new Census__c();
            objCensus.Id = objOldCensusMember.vlocity_ins_CensusId__c;
            objCensus.Primary_Member__c = NULL;
            lstCensusToUpdate.add(objCensus);
            }
        }
        if(!lstCensusToUpdate.isEmpty()){
            update lstCensusToUpdate;
        }
    }

/*
@ Method name       :   beforeInsertCensusMember
@ Created by        :   Govardhini
@ Created on        :   03-01-2021
@ Token/Jira Ticket :   SLIV - 62
@ Description       :   Task - 06 Inser/Update Trigger Validation. If a Census Member Record is Inserted/Updated with Effective Start Date and Effective Term Date,
it should be within the Range of Census objects Effective start date and End Date else it should Throw an error.  
*/
    public void beforeInsertCensusMember(List<Census_Member__c> lstNewCensusMember){
        // When I create/Update a Census Member record, Effective date and Term Date should be within the parent Census record’s Effective and Term Dates. 
        Map<Id, Census_Member__c> mapOfCensusIdToCensusMember = new  Map<Id, Census_Member__c>();            
        for(Census_Member__c objNewCensusMember : lstNewCensusMember){            
            mapOfCensusIdToCensusMember.put(objNewCensusMember.vlocity_ins_CensusId__c, objNewCensusMember);            
        }
        Map<Id, Census__c> mapCensus =new Map<Id, Census__c> ([SELECT Id, vlocity_ins_EffectiveStartDate__c, vlocity_ins_EffectiveEndDate__c FROM Census__c WHERE Id IN :mapOfCensusIdToCensusMember.keySet()]);
        for(Id censusId : mapOfCensusIdToCensusMember.keySet()){           
            if(mapCensus.get(censusId).vlocity_ins_EffectiveStartDate__c <= mapOfCensusIdToCensusMember.get(censusId).htcs_EffectiveDate__c 
            && mapOfCensusIdToCensusMember.get(censusId).htcs_EffectiveDate__c <= mapCensus.get(censusId).vlocity_ins_EffectiveEndDate__c
            && mapOfCensusIdToCensusMember.get(censusId).htcs_EffectiveDate__c <= mapOfCensusIdToCensusMember.get(censusId).htcs_TermDate__c
            && mapCensus.get(censusId).vlocity_ins_EffectiveStartDate__c <= mapOfCensusIdToCensusMember.get(censusId).htcs_TermDate__c
            && mapOfCensusIdToCensusMember.get(censusId).htcs_TermDate__c <= mapCensus.get(censusId).vlocity_ins_EffectiveEndDate__c 
            || (mapCensus.get(censusId).vlocity_ins_EffectiveStartDate__c == NULL && mapCensus.get(censusId).vlocity_ins_EffectiveEndDate__c == NULL 
            && mapOfCensusIdToCensusMember.get(censusId).htcs_EffectiveDate__c == NULL && mapOfCensusIdToCensusMember.get(censusId).htcs_TermDate__c == NULL)){                
            }
            else{
                mapOfCensusIdToCensusMember.get(censusId).addError('Please Enter the Valid Date');
            }                       
        }
    }    
/*
@ Method name       :   beforInsertOfCensusMemberWithPrimary
@ Created by        :   Govardhini
@ Created on        :   03-01-2021
@ Token/Jira Ticket :   SLIV - 62
@ Description       :   Task - 07 Insert/Update Trigger Validation. If the Census Member is Inserted/Updated with Primary check it needs to,
Check there will be only one primary Census member per Census record else throw an error.  
*/
    public void beforInsertOfCensusMemberWithPrimary(List<Census_Member__c> lstNewCensusMember){
        // When I create/Update a Census Member record, Check there will be only one primary Census member per Census record.
        map<Id, Census_Member__c> mapCensusMemberToCensusId = new Map<Id, Census_Member__c>();
        for(Census_Member__c objCensusMember : lstNewCensusMember){
            if(objCensusMember.vlocity_ins_IsPrimaryMember__c == TRUE){
                mapCensusMemberToCensusId.put(objCensusMember.vlocity_ins_CensusId__c, objCensusMember);
            }
        }
        Map<Id, Census__c> mapCensus = new Map<Id, Census__c>([SELECT Id, Primary_Member__c FROM Census__c WHERE Id IN :mapCensusMemberToCensusId.keySet()]);
        for(Id censusId : mapCensusMemberToCensusId.keySet()){
            if(mapCensus.get(censusId).Primary_Member__c != NULL){
                mapCensusMemberToCensusId.get(censusId).vlocity_ins_IsPrimaryMember__c.addError('Primary Member Already Exist');
            }
        }
    }
/*
@ Method name       :   afterInsertCensusMember
@ Created by        :   Govardhini
@ Created on        :   03-01-2021
@ Token/Jira Ticket :   SLIV - 62
@ Description       :   Task - 08 Insert. Census Member record Should rollup or append all the census member name’s to its parent Census record.  
*/    
    public void afterInsertCensusMember(List<Census_Member__c> lstNewCensusMember){
        // When I create a Census Member record, I need to rollup or append all the census member name’s to its parent Census record.
        Map<Id, String> mapToUpdateCensuswithCensusMemberName = new Map<Id, String>();
        Set<Id> setCensusId = new set<Id>();
        for(Census_Member__c objCensusMember : lstNewCensusMember){
            setCensusId.add(objCensusMember.vlocity_ins_CensusId__c);
        }

        for(Census_Member__c objCensusMember : [SELECT Id, vlocity_ins_CensusId__c, Name FROM Census_Member__c WHERE vlocity_ins_CensusId__c IN :setCensusId]){
            if(!(mapToUpdateCensuswithCensusMemberName.containsKey(objCensusMember.vlocity_ins_CensusId__c))){
                mapToUpdateCensuswithCensusMemberName.put(objCensusMember.vlocity_ins_CensusId__c, objCensusMember.Name);
            }
            else{
                mapToUpdateCensuswithCensusMemberName.put(objCensusMember.vlocity_ins_CensusId__c, mapToUpdateCensuswithCensusMemberName.get(objCensusMember.vlocity_ins_CensusId__c)+'; '+objCensusMember.Name);
            }
        }

        List<Census__c> lstCensusToUpdate = new List<Census__c>();
        for(Id censusId : mapToUpdateCensuswithCensusMemberName.keySet()){
            Census__c objCensus = new Census__c();
            objCensus.Id = censusId;
            objCensus.Members__c = mapToUpdateCensuswithCensusMemberName.get(censusId);
            lstCensusToUpdate.add(objCensus);
        }
        if(!lstCensusToUpdate.isEmpty()){
            Update lstCensusToUpdate;
        }
    }
    
/*
@ Method name       :   afterUpdateCensusMember
@ Created by        :   Govardhini
@ Created on        :   03-01-2021
@ Token/Jira Ticket :   SLIV - 62
@ Description       :   Task - 08 Update. Census Member record Should rollup or append all the census member name’s to its parent Census record.  
*/    
    public void afterUpdateCensusMember(Map<Id, Census_Member__c> mapOldIdToCensusMember, Map<Id, Census_Member__c> mapNewIdToCensusMember){
        // When I update a Census Member record, I need to rollup or append all the census member name’s to its parent Census record.
        Map<Id, String> mapToUpdateCensuswithCensusMemberName = new Map<Id, String>();
        Set<Id> setCensusId = new Set<Id>();
        for(Id censusMemberId : mapNewIdToCensusMember.keySet()){
            if(mapOldIdToCensusMember.get(censusMemberId).Name != mapNewIdToCensusMember.get(censusMemberId).Name){
                setCensusId.add(mapNewIdToCensusMember.get(censusMemberId).vlocity_ins_CensusId__c);
            }
        }
        for(Census_Member__c objCensusMember : [SELECT Id, vlocity_ins_CensusId__c, Name FROM Census_Member__c WHERE vlocity_ins_CensusId__c IN :setCensusId]){
            if(!(mapToUpdateCensuswithCensusMemberName.containsKey(objCensusMember.vlocity_ins_CensusId__c))){
                mapToUpdateCensuswithCensusMemberName.put(objCensusMember.vlocity_ins_CensusId__c, objCensusMember.Name);
            }
            else{
                mapToUpdateCensuswithCensusMemberName.put(objCensusMember.vlocity_ins_CensusId__c, mapToUpdateCensuswithCensusMemberName.get(objCensusMember.vlocity_ins_CensusId__c)+'; '+objCensusMember.Name);
            }
        }
        List<Census__c> lstCensusToUpdate = new List<Census__c>();
        for(Id censusId : mapToUpdateCensuswithCensusMemberName.keySet()){
            Census__c objCensus = new Census__c();
            objCensus.Id = censusId;
            objCensus.Members__c = mapToUpdateCensuswithCensusMemberName.get(censusId);
            lstCensusToUpdate.add(objCensus);
        }
        if(!lstCensusToUpdate.isEmpty()){
            Update lstCensusToUpdate;
        }
    }
}