@isTest
public class JewelryHandlerTest {
    @testSetup
    static void makeData(){

        List<Jewelry__c> lstJewelry = new List<Jewelry__c>();
        for(Integer i=0; i<=50; i++){
            Jewelry__c objJewelry = new Jewelry__c();
            objJewelry.Name = 'Jewelry'+i;
            objJewelry.Quantity__c = 5;
            objJewelry.Unit_Price__c = 40;
            lstJewelry.add(objJewelry);
        }
        if(!lstJewelry.isEmpty()){
        Insert lstJewelry;
        }

        Jewelry__c objJewlry = [SELECT Id, Unit_Price__c, Total_Price__c, Quantity__c FROM Jewelry__c LIMIT 1];
        System.assertEquals(200, objJewlry.Total_Price__c, 'Jewelry Should have Total_Price Field');

        List<Jewelry__c> lstToUpdateJewelry = new List<Jewelry__c>();
        for(Integer j=0; j<=50; j++){
            lstJewelry[j].Unit_Price__c = 10;
            lstToUpdateJewelry.add(lstJewelry[j]);
        }
        if(!lstToUpdateJewelry.isEmpty()){
            update lstToUpdateJewelry;
        }
    }

    @isTest static void classCallingMethod(){
        Test.startTest();
        Jewelry__c objJewlry1 = [SELECT Id, Unit_Price__c, Total_Price__c, Quantity__c FROM Jewelry__c LIMIT 1];
        System.assertEquals(50, objJewlry1.Total_Price__c, 'Jewelry Should have Total_Price Field');
        Test.stopTest();            
         }
        
        

   
}
