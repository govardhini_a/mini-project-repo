/*
@ Class name        :   JewelryHandler.cls
@ Created by        :   Govardhini
@ Created on        :   03-01-2021
@ Token/Jira Ticket :   SLIV - 62
@ Description       :   Trigger Handler class of Jewelry object. 
*/
public class JewelryHandler {
    public static void onBeforeInsert(List<Jewelry__c> lstNewJewelry){
        JewelryHandler objJewelryHandler = new JewelryHandler();
        objJewelryHandler.insertJewelry(lstNewJewelry);
    }

    public static void onBeforeUpdate(Map<Id, Jewelry__c> mapOldIdToJewelry, Map<Id, Jewelry__c> mapNewIdToJewelry){
        JewelryHandler objJewelryHandler = new JewelryHandler();
        objJewelryHandler.updateJewelry(mapOldIdToJewelry, mapNewIdToJewelry);
    }
/*
@ Method name       :   insertJewelry
@ Created by        :   Govardhini
@ Created on        :   03-01-2021
@ Token/Jira Ticket :   SLIV - 62
@ Description       :   Task - 03 Insert Case. If a Jewelry object is Inserted Total Price should be calculated. 
*/
    public void insertJewelry(List<Jewelry__c> lstNewJewelry){        
        for(Jewelry__c objNewJewelry : lstNewJewelry){
            if(objNewJewelry.Unit_Price__c != NULL && objNewJewelry.Quantity__c != NULL){
                objNewJewelry.Total_Price__c = objNewJewelry.Unit_Price__c * objNewJewelry.Quantity__c;
            }
        }        
    }
/*
@ Method name       :   updateJewelry
@ Created by        :   Govardhini
@ Created on        :   03-01-2021
@ Token/Jira Ticket :   SLIV - 62
@ Description       :   Task - 03 Update Case. If a Jewelry object is Updated Total Price should be calculated. 
*/
    public void updateJewelry(Map<Id, Jewelry__c> mapOldIdToJewelry, Map<Id, Jewelry__c> mapNewIdToJewelry){        
        for(Id jewelryId : mapNewIdToJewelry.keySet()){
            if((mapNewIdToJewelry.get(jewelryId).Unit_Price__c != NULL) && (mapNewIdToJewelry.get(jewelryId).Quantity__c != NULL)){
                if(((mapOldIdToJewelry.get(jewelryId).Unit_Price__c != mapNewIdToJewelry.get(jewelryId).Unit_Price__c) || (mapOldIdToJewelry.get(jewelryId).Quantity__c != mapNewIdToJewelry.get(jewelryId).Quantity__c)) || ((mapOldIdToJewelry.get(jewelryId).Unit_Price__c != mapNewIdToJewelry.get(jewelryId).Unit_Price__c) && (mapOldIdToJewelry.get(jewelryId).Quantity__c != mapNewIdToJewelry.get(jewelryId).Quantity__c))){
                    mapNewIdToJewelry.get(jewelryId).Total_Price__c = mapNewIdToJewelry.get(jewelryId).Unit_Price__c * mapNewIdToJewelry.get(jewelryId).Quantity__c;
                }
            }
        }       
    }
}
