@isTest
public class CensusMemberHandlerTest {       
    Static testMethod void censusMemberInsert(){
        Account objAccount = new Account();
        objAccount.Name = 'Account';
        insert objAccount; 
        
        Census__c objCensus = new Census__c();
        objCensus.Name = 'Census';
        objCensus.vlocity_ins_GroupId__c = objAccount.Id;                
        insert objCensus;

        Census_Member__c objCensusMember = new Census_Member__c();
        objCensusMember.Name = 'Census Member';
        objCensusMember.vlocity_ins_CensusId__c = objCensus.Id;
        objCensusMember.vlocity_ins_IsPrimaryMember__c = TRUE;
        insert objCensusMember;        
                  
        Census__c objCensus1 = [SELECT Id, Primary_Member__c FROM Census__c LIMIT 1];
        Census_Member__c objCensusMember1 = [SELECT Name, vlocity_ins_IsPrimaryMember__c FROM Census_Member__c LIMIT 1];
        System.assertEquals(objCensusMember1.Name, objCensus1.Primary_Member__c);        
    }

    Static testMethod void censusMemberUpdateForTrue(){
        Account objAccount = new Account();
        objAccount.Name = 'Account';
        insert objAccount;      
        
        Census__c objCensus = new Census__c();
        objCensus.vlocity_ins_GroupId__c = objAccount.Id;
        objCensus.Name = 'Census';
        insert objCensus;        

        List<Census_Member__c> lstCensusMember = new List<Census_Member__c>();       
        
        Census_Member__c objCensusMember1 = new Census_Member__c();
            objCensusMember1.Name = 'Census Member 1';
            objCensusMember1.vlocity_ins_CensusId__c = objCensus.Id;
            objCensusMember1.vlocity_ins_IsPrimaryMember__c = FALSE;
            lstCensusMember.add(objCensusMember1);

        Census_Member__c objCensusMember2 = new Census_Member__c();
            objCensusMember2.Name = 'Census Member 1';
            objCensusMember2.vlocity_ins_CensusId__c = objCensus.Id;
            objCensusMember2.vlocity_ins_IsPrimaryMember__c = FALSE;
            lstCensusMember.add(objCensusMember2);        
        
        if(!lstCensusMember.isEmpty()){
            insert lstCensusMember;
        }      
        
        objCensusMember1.vlocity_ins_IsPrimaryMember__c = TRUE;
        update objCensusMember1;        

        Census__c objCensus1 = [SELECT Id, Primary_Member__c FROM Census__c LIMIT 1];
        Census_Member__c objCensusMember4 = [SELECT Name, vlocity_ins_IsPrimaryMember__c FROM Census_Member__c LIMIT 1];
        System.assertEquals(objCensusMember4.Name, objCensus1.Primary_Member__c);
        
    }

    Static testMethod void censusMemberUpdateForFalse(){
        Account objAccount = new Account();
        objAccount.Name = 'Account';
        insert objAccount;

        Census__c objCensus2 = new Census__c();
        objCensus2.vlocity_ins_GroupId__c = objAccount.Id;
        objCensus2.Name = 'Census 2';
        insert objCensus2;

        Census_Member__c objCensusMember3 = new Census_Member__c();
        objCensusMember3.Name = 'Census Member 3';
        objCensusMember3.vlocity_ins_CensusId__c = objCensus2.Id;
        objCensusMember3.vlocity_ins_IsPrimaryMember__c = TRUE;
        insert objCensusMember3;

        objCensusMember3.vlocity_ins_IsPrimaryMember__c = FALSE;
        update objCensusMember3;

        Census__c objCensus1 = [SELECT Id, Primary_Member__c FROM Census__c LIMIT 1];
        Census_Member__c objCensusMember4 = [SELECT Name, vlocity_ins_IsPrimaryMember__c FROM Census_Member__c LIMIT 1];
        System.assertEquals(NULL, objCensus1.Primary_Member__c);

    }

    Static testMethod void censusMemberDelete(){
        Account objAccount = new Account();
        objAccount.Name = 'Account';
        insert objAccount;

        Census__c objCensus = new Census__c();
        objCensus.Name = 'New Census';
        objCensus.vlocity_ins_GroupId__c = objAccount.Id;
        insert objCensus;
        
        List<Census_Member__c> lstCensusMember = new List<Census_Member__c>();
        Census_Member__c objCensusMember1 = new Census_Member__c();
        objCensusMember1.Name = 'New Census Member';
        objCensusMember1.vlocity_ins_CensusId__c = objCensus.Id;
        objCensusMember1.vlocity_ins_IsPrimaryMember__c = TRUE;
        lstCensusMember.add(objCensusMember1);

        Census_Member__c objCensusMember2 = new Census_Member__c();
        objCensusMember2.Name = 'New Census Member 2';
        objCensusMember2.vlocity_ins_CensusId__c = objCensus.Id;
        objCensusMember2.vlocity_ins_IsPrimaryMember__c = FALSE;
        lstCensusMember.add(objCensusMember2);

        if(!lstCensusMember.isEmpty()){
            insert lstCensusMember;
        }

        delete objCensusMember1;
        
        Census__c objCensus1 = [SELECT Id, Primary_Member__c FROM Census__c LIMIT 1];
        //Census_Member__c objCensusMember1 = [SELECT Name, vlocity_ins_IsPrimaryMember__c FROM Census_Member__c LIMIT 1];
        System.assertEquals(NULL, objCensus1.Primary_Member__c);        
    }

    static testMethod void censusMemberInsertForDate(){ // Insert Check for the Date Task 
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;

        Census__c objCensus = new Census__c();
        objCensus.Name = 'Test Census';
        objCensus.vlocity_ins_GroupId__c = objAccount.Id;
        objCensus.vlocity_ins_EffectiveStartDate__c = Date.newInstance(2016, 12, 12);
        objCensus.vlocity_ins_EffectiveEndDate__c = Date.newInstance(2017, 12, 12);
        insert objCensus;

        List<Census_Member__c> lstCensusMember = new List<Census_Member__c>();

        for(Integer i=0; i<=10; i++){
            Census_Member__c objCensusMember = new Census_Member__c();
            objCensusMember.Name = 'New Member'+i;
            objCensusMember.vlocity_ins_CensusId__c = objCensus.id;
            objCensusMember.htcs_EffectiveDate__c = Date.newInstance(2016, 05, 05);
            objCensusMember.htcs_TermDate__c = Date.newInstance(2017, 02, 02);
            lstCensusMember.add(objCensusMember);
        }
        try{
            if(!lstCensusMember.isEmpty()){
                insert lstCensusMember;
            }
        }
        catch(Exception e){
            Boolean errorMessageException = e.getMessage().contains('Please Enter the Valid Date')?true: false;
            System.assertEquals(true, errorMessageException);
        }
    }

    static testMethod void censusMemberUpdateForDate(){
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;

        Census__c objCensus = new Census__c();
        objCensus.Name = 'Test Census';
        objCensus.vlocity_ins_GroupId__c = objAccount.Id;
        objCensus.vlocity_ins_EffectiveStartDate__c = Date.newInstance(2016, 12, 12);
        objCensus.vlocity_ins_EffectiveEndDate__c = Date.newInstance(2017, 12, 12);
        insert objCensus;

        List<Census_Member__c> lstCensusMember = new List<Census_Member__c>();

        for(Integer i=0; i<=10; i++){
            Census_Member__c objCensusMember = new Census_Member__c();
            objCensusMember.Name = 'New Member'+i;
            objCensusMember.vlocity_ins_CensusId__c = objCensus.id;
            objCensusMember.htcs_EffectiveDate__c = Date.newInstance(2017, 01, 01);
            objCensusMember.htcs_TermDate__c = Date.newInstance(2017, 02, 02);
            lstCensusMember.add(objCensusMember);
        } 
        if(!lstCensusMember.isEmpty()){
            insert lstCensusMember;
        }
        
        for(Census_Member__c objCensusMember : lstCensusMember){
            objCensusMember.htcs_EffectiveDate__c = Date.newInstance(2016, 05, 05);
            objCensusMember.htcs_TermDate__c = Date.newInstance(2017, 02, 02);
        }
        try{
            if(!lstCensusMember.isEmpty()){
                update lstCensusMember;
            }
        }
        catch(Exception e){
            Boolean errorMessageException = e.getMessage().contains('Please Enter the Valid Date')?true: false;
            System.assertEquals(true, errorMessageException);
        }

    }

    static testMethod void censusMemberInsertForPrimary(){
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;

        Census__c objcensus = new Census__c();
        objcensus.Name = 'Test Census';
        objCensus.vlocity_ins_GroupId__c = objAccount.Id;        
        insert objCensus;

        Census_Member__c objCensusMember = new Census_Member__c();
        objCensusMember.Name = 'Census Member 1';
        objCensusMember.vlocity_ins_CensusId__c = objcensus.Id;
        objCensusMember.vlocity_ins_IsPrimaryMember__c = TRUE;        
        insert objCensusMember;               

        try{
        Census_Member__c objCensusMember1 = new Census_Member__c();
        objCensusMember1.Name = 'Census Member 2';
        objCensusMember1.vlocity_ins_CensusId__c = objcensus.Id;
        objCensusMember1.vlocity_ins_IsPrimaryMember__c = TRUE;
        insert objCensusMember1;            
        }
        catch(Exception e){
            Boolean errorMessageException = e.getMessage().contains('Primary Member Already Exist')? true : false;
            System.assertEquals(true, errorMessageException);
        }
    }

    static testMethod void censusMemberUpdateForPrimary(){
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;

        Census__c objcensus = new Census__c();
        objcensus.Name = 'Test Census';
        objCensus.vlocity_ins_GroupId__c = objAccount.Id;        
        insert objCensus;

        Census_Member__c objCensusMember = new Census_Member__c();
        objCensusMember.Name = 'Census Member 1';
        objCensusMember.vlocity_ins_CensusId__c = objcensus.Id;
        objCensusMember.vlocity_ins_IsPrimaryMember__c = TRUE;        
        insert objCensusMember; 
        
        Census_Member__c objCensusMember1 = new Census_Member__c();
        objCensusMember1.Name = 'Census Member 2';
        objCensusMember1.vlocity_ins_CensusId__c = objcensus.Id;
        objCensusMember1.vlocity_ins_IsPrimaryMember__c = FALSE;
        insert objCensusMember1;

        try{
            objCensusMember1.vlocity_ins_IsPrimaryMember__c = TRUE;
            update objCensusMember1;                    
        }
        catch(Exception e){
            Boolean errorMessageException = e.getMessage().contains('Primary Member Already Exist')? true : false;
            System.assertEquals(true, errorMessageException);
        }
    }

    static testMethod void censusMemberInsertForCensusMemberField(){
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;

        Census__c objcensus = new Census__c();
        objcensus.Name = 'Test Census';
        objCensus.vlocity_ins_GroupId__c = objAccount.Id;        
        insert objCensus;

        List<Census_Member__c> lstCensusMember = new List<Census_Member__c>();
        for(Integer i=0; i<5; i++){
            Census_Member__c objCensusMember = new Census_Member__c();
            objCensusMember.Name = 'Census Member '+i;
            objCensusMember.vlocity_ins_CensusId__c = objcensus.Id;                
            lstCensusMember.add(objCensusMember);
        }
        if(!lstCensusMember.isEmpty()){
            insert lstCensusMember;
        }

        Census__c objCensus1 = [SELECT Id, Members__c FROM Census__c LIMIT 1];

        System.assertEquals('Census Member 0; Census Member 1; Census Member 2; Census Member 3; Census Member 4', objCensus1.Members__c);        
    }

    static testMethod void censusMemberUpdateForCensusMemberField(){
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;

        Census__c objcensus = new Census__c();
        objcensus.Name = 'Test Census';
        objCensus.vlocity_ins_GroupId__c = objAccount.Id;        
        insert objCensus;

        List<Census_Member__c> lstCensusMember = new List<Census_Member__c>();
        for(Integer i=0; i<5; i++){
            Census_Member__c objCensusMember = new Census_Member__c();
            objCensusMember.Name = 'Census Member '+i;
            objCensusMember.vlocity_ins_CensusId__c = objcensus.Id;                
            lstCensusMember.add(objCensusMember);
        }
        if(!lstCensusMember.isEmpty()){
            insert lstCensusMember;
        }

        for(Census_Member__c objCensusMember : lstCensusMember){
            objCensusMember.Name = objCensusMember.Name +' Welcome';
        }

        if(!lstCensusMember.isEmpty()){
            update lstCensusMember;
        }

        Census__c objCensus1 = [SELECT Id, Members__c FROM Census__c LIMIT 1];

        System.assertEquals('Census Member 0 Welcome; Census Member 1 Welcome; Census Member 2 Welcome; Census Member 3 Welcome; Census Member 4 Welcome', objCensus1.Members__c);        
    }
}
