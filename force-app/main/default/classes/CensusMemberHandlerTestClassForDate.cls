@isTest
public with sharing class CensusMemberHandlerTestClassForDate {

    static testMethod void censusMemberInsert(){
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;

        Census__c objCensus = new Census__c();
        objCensus.Name = 'Test Census';
        objCensus.vlocity_ins_GroupId__c = objAccount.Id;
        objCensus.vlocity_ins_EffectiveStartDate__c = Date.newInstance(2016, 12, 12);
        objCensus.vlocity_ins_EffectiveEndDate__c = Date.newInstance(2017, 12, 12);
        insert objCensus;

        List<Census_Member__c> lstCensusMember = new List<Census_Member__c>();

        for(Integer i=0; i<=10; i++){
            Census_Member__c objCensusMember = new Census_Member__c();
            objCensusMember.Name = 'New Member'+i;
            objCensusMember.vlocity_ins_CensusId__c = objCensus.id;
            objCensusMember.htcs_EffectiveDate__c = Date.newInstance(2016, 05, 05);
            objCensusMember.htcs_TermDate__c = Date.newInstance(2017, 02, 02);
            lstCensusMember.add(objCensusMember);
        }
        try{
            if(!lstCensusMember.isEmpty()){
                insert lstCensusMember;
            }
        }
        catch(Exception e){
            Boolean errorMessageException = e.getMessage().contains('Please Enter the Valid Date')?true: false;
            System.assertEquals(errorMessageException, true);
        }

        

        // for(Census_Member__c objCensusMember : lstCensusMember){
        //     objCensusMember.htcs_EffectiveDate__c = Date.newInstance(2016, 12, 12);
        //     objCensusMember.htcs_TermDate__c = Date.newInstance(2017, 12, 12);
        // }

        // if(!lstCensusMember.isEmpty()){
        //     update lstCensusMember;
        // }

        // Census_Member__c objCensusMember1 =[SELECT Id, htcs_EffectiveDate__c, htcs_TermDate__c FROM Census_Member__c LIMIT 1]; 

        // System.assertEquals(Date.newInstance(2016, 12, 12), objCensusMember1.htcs_EffectiveDate__c);


    }
    
}
